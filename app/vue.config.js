module.exports = {
  devServer: {
    port: process.env.EXPOSED_PORT
  },
  transpileDependencies: [
    'vuetify'
  ]
}

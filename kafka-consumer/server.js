//require('dotenv').config();
const WebSocketServer = require('websocket').server;
const { Kafka, logLevel } = require('kafkajs');
const http = require('http');
const {
  KAFKA_HOST,
  KAFKA_TOPIC,
  EXPOSED_PORT
} = process.env;


const host = `${KAFKA_HOST}`; // process.env.HOST_IP || ip.address()
const exposedPort = `${EXPOSED_PORT}`;

const kafka = new Kafka({
    logLevel: logLevel.INFO,
    brokers: [host],
    clientId: 'kafkajs-consumer'
});




const server = http.createServer(function(request, response) {
    console.log('Request recieved: ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(`${exposedPort}`, function() {
    console.log(`Listening on port: ${exposedPort}`);
});
    
const webSocketServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
    
function iSOriginAllowed(origin) {
    return true;
}
    
webSocketServer.on('request', function(request) {
    if (!iSOriginAllowed(request.origin)) {
        request.reject();
        console.log('Connection from: ' + request.origin + ' rejected.');
        return;
    }
    
    const topicList = `${KAFKA_TOPIC}`;
    const topics = topicList.split(',');
    const topic1 = 'cic_selection_evaluation';
    const topic2 = 'cic_vulnerability_stats';
    const topic3 = 'cic_model_results';
    const connection = request.accept('echo-protocol', request.origin);
    console.log('Connection accepted: ' + request.origin);

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
        }
    });

    

    const consumer = kafka.consumer({ groupId: 'kafkajs-group' });
    
    const run = async () => {
        await consumer.connect();
        await consumer.subscribe({ topics: topics, fromBeginning: true });
        // await consumer.subscribe({ topics: ['cic_selection_evaluation', 'cic_vulnerability_stats', 'cic_model_results'], fromBeginning: true });
        // await consumer.subscribe({ topics: `${topic1}`, fromBeginning: true });
        // await consumer.subscribe({ topic2, fromBeginning: true });
        // await consumer.subscribe({ topic3, fromBeginning: true });

        await consumer.run({
            eachMessage: async ({ topic, partition, message }) => {
                const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`;
                console.log(`- ${prefix} ${message.key}#${message.value}`);

                let strToSend = {
                    topicName: topic,
                    key: message.key.toString(),
                    value: message.value.toString()
                };
                connection.sendUTF(JSON.stringify(strToSend));
                // connection.sendUTF(message.value);
            },
        });

        topics.forEach(tp => {
            consumer.seek({ topic: tp, partition: 0, offset: -2 });
        });
    }
    
    run().catch(e => console.error(`[example/consumer] ${e.message}`, e));




    connection.on('close', function(reasonCode, description) {
        console.log('Connection ' + connection.remoteAddress + ' disconnected.');
    });
});

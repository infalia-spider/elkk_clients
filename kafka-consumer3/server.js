//require('dotenv').config();
const WebSocketServer = require('websocket').server;
const { Kafka, logLevel } = require('kafkajs');
const http = require('http');
const {
  KAFKA_HOST,
  KAFKA_TOPIC,
  EXPOSED_PORT
} = process.env;


const host = `${KAFKA_HOST}`; // process.env.HOST_IP || ip.address()
const exposedPort = `${EXPOSED_PORT}`;

const kafka = new Kafka({
    logLevel: logLevel.INFO,
    brokers: [host],
    clientId: 'kafkajs-consumer3'
});




const server = http.createServer(function(request, response) {
    console.log('Request recieved: ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(`${exposedPort}`, function() {
    console.log(`Listening on port: ${exposedPort}`);
});
    
webSocketServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
    
function iSOriginAllowed(origin) {
    return true;
}
    
webSocketServer.on('request', function(request) {
    if (!iSOriginAllowed(request.origin)) {
        request.reject();
        console.log('Connection from: ' + request.origin + ' rejected.');
        return;
    }
    
    const connection = request.accept('echo-protocol3', request.origin);
    console.log('Connection accepted: ' + request.origin);

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
        }
    });




    const topic = `${KAFKA_TOPIC}`;
    const consumer = kafka.consumer({ groupId: 'test-group3' });

    const run = async () => {
        await consumer.connect();
        await consumer.subscribe({ topic, fromBeginning: true });
        await consumer.run({
            // eachBatch: async ({ batch }) => {
            //   console.log(batch)
            // },
            eachMessage: async ({ topic, partition, message }) => {
                const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`
                console.log(`- ${prefix} ${message.key}#${message.value}`)

                connection.sendUTF(message.value);
            },
        });

        consumer.seek({ topic: topic, partition: 0, offset: -2 });
    }
    
    run().catch(e => console.error(`[example/consumer3] ${e.message}`, e))




    connection.on('close', function(reasonCode, description) {
        console.log('Connection ' + connection.remoteAddress + ' disconnected.');
    });
});

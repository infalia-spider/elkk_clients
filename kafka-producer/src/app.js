const path = require("path");
const favicon = require("serve-favicon");
const compress = require("compression");
const helmet = require("helmet");
const cors = require("cors");
const logger = require("./logger");

const feathers = require("@feathersjs/feathers");
const configuration = require("@feathersjs/configuration");
const express = require("@feathersjs/express");
const swagger = require("feathers-swagger");

const middleware = require("./middleware");
const services = require("./services");
const appHooks = require("./app.hooks");
const channels = require("./channels");

///// const authentication = require("./authentication");

const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get("public"), "favicon.ico")));
// Host the public folder
app.use("/", express.static(app.get("public")));

// Set up Plugins and providers
app.configure(express.rest());

// Set up swagger
app.configure(
  swagger({
    swagger: "2.0",
    openApiVersion: 3,
    docsPath: "/api",
    docsJsonPath: "/jsonapi",
    uiIndex: true,
    schemes: ["http", "https"],
    specs: {
      info: {
        title: "Kafka Producer Middleware API",
        description:
          'The purpose of this middleware is to support SPIDER components that cannot interact directly with Kafka to post their messages via REST. This middleware is implemented in the context of T3.5 (Data Collection and Visualisation Toolkit). The API is compatible with the latest OpenAPI Specification - Version 3.<p><a href="/jsonapi">Get the schema in JSON</a></p>',
        version: "0.31",
      },
      servers: [
        {
          url: "https://spider-api.euprojects.net",
          description: "Spider server",
        }
        // {
        //   url: "http://{environment}.local:3030",
        //   description: "Production server",
        //   variables: {
        //     environment: {
        //       enum: ["dev", "staging", "production"],
        //       default: "dev",
        //     },
        //   },
        // },
      ],

      components: {
        // securitySchemes: {
        //   bearer: {
        //     type: "http",
        //     scheme: "bearer",
        //     bearerFormat: "JWT",
        //     description:
        //       "To get a bearer token you need to authenticate by calling POST/authentication",
        //   },
        // },
        // schemas: {
        //   authentication: {
        //     type: "object",
        //     properties: {
        //       email: {
        //         type: "string",
        //         description: "Email used to log in",
        //       },
        //       password: {
        //         type: "string",
        //         description: "Password for the specific user",
        //       },
        //       strategy: {
        //         type: "string",
        //         enum: ["local"],
        //         description:
        //           "Authentication strategy is always local unless login with Google",
        //       },
        //     },
        //   },
        //   authenticationResponse: {
        //     type: "object",
        //     properties: {
        //       accessToken: {
        //         type: "string",
        //         description:
        //           "A JWT authorisation token to access restricted resources",
        //       },
        //       authentication: {
        //         type: "object",
        //         properties: {
        //           strategy: {
        //             type: "string",
        //             description: "",
        //           },
        //           payload: {
        //             type: "string",
        //             description: "",
        //           },
        //         },
        //       },
        //       user: {
        //         type: "object",
        //         properties: {
        //           _id: {
        //             type: "string",
        //             description: "User ID",
        //           },
        //           email: {
        //             type: "string",
        //             description: "User email",
        //           },
        //           createdAt: {
        //             type: "string",
        //             description: "The ISODate where user is created",
        //           },
        //           updatedAt: {
        //             type: "string",
        //             description: "The ISODate where user is updated",
        //           },
        //         },
        //       },
        //     },
        //   },
        // },
      },
      // paths: {
      //   "/authentication": {
      //     post: {
      //       tags: ["authentication"],
      //       summary: "Get new JWT",
      //       description:
      //         "Get new JWT to access restricted routes on LCA Calculator API based on given user info",
      //       responses: {
      //         201: {
      //           description: "JWT returned",
      //           content: {
      //             "application/json": {
      //               schema: {
      //                 $ref: "#/components/schemas/authenticationResponse",
      //               },
      //             },
      //           },
      //         },
      //         401: {
      //           description: "Unauthorized",
      //         },
      //       },
      //       requestBody: {
      //         description: "User information for login",
      //         required: true,
      //         content: {
      //           "application/json": {
      //             schema: {
      //               $ref: "#/components/schemas/authentication",
      //             },
      //           },
      //         },
      //       },
      //     },
      //   },
      // },
      // security: [
      //   {
      //     bearer: [],
      //   },
      // ],
    },
    // ignore: {
    //   paths: ["authentication"],
    // },
    defaults: {
      operations: {
        find: {
          "parameters[]": {
            description: "Property to query results",
            in: "query",
            name: "$select[]",
            schema: {
              type: "array",
              items: {
                type: "string",
              },
            },
            allowReserved: true,
          },
        },
        get: {
          parameters: [
            {
              in: "path",
              name: "_id",
              required: true,
              schema: {
                type: "string",
              },
              description: "The ObjectId to retrieve",
            },
          ],
        },
        update: {
          parameters: [
            {
              in: "path",
              name: "_id",
              required: true,
              schema: {
                type: "string",
              },
              description: "The ObjectId to be updated",
            },
          ],
        },
        patch: {
          parameters: [
            {
              in: "path",
              name: "_id",
              required: true,
              schema: {
                type: "string",
              },
              description: "The ObjectId to be patched",
            },
          ],
        },
        remove: {
          parameters: [
            {
              in: "path",
              name: "_id",
              required: true,
              schema: {
                type: "string",
              },
              description: "The ObjectId to be permanently deleted",
            },
          ],
        },
      },
    },
  })
);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
///// app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;

const { Kafka, logLevel, CompressionTypes } = require('kafkajs');
const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  KAFKA_HOST
} = process.env;


const createKafkaMessage = () => {
  return async context => {
    const { data } = context;


    if(!data.topic) {
      throw new Error('Topic is missing');
    }

    if(!data.payload) {
      throw new Error('Payload is missing');
    }
    
    
    
//     const msg = '';
//     const payload = data.payload;
//     
//     if (typeof (payload) === 'string') {
//         msg = payload
//     } else if (Object.prototype.toString.call(payload) === '[object Object]') {
//       msg = Object.assign({}, msg, payload);
//     } else if (Object.prototype.toString.call(payload) === '[object Array]') {
//         let dataArr = [];
//         for (let i = 0; i < payload.length; i++) {
//             dataArr[i] = Object.assign({}, producer, payload[i]);
//         }
//         
//         producers = dataArr;
//     }



    const payloadStr = JSON.stringify(data.payload);
    
    const msg = data.payload; // `${payloadStr}`;
    const topic = `${data.topic}`; // `${KAFKA_TOPIC}`;


    const host = `${KAFKA_HOST}`;

    const kafka = new Kafka({
        logLevel: logLevel.INFO,
        brokers: [host],
        clientId: 'example-producer'
    });


    const producer = kafka.producer()

    const getRandomNumber = () => Math.round(Math.random(10) * 10000)
    
    const sendMessage = () => {
        return producer
            .send({
                topic,
                compression: CompressionTypes.GZIP,
                messages: [
                  {
                    key: `key-${getRandomNumber()}`,
                    value: JSON.stringify({
                        time: `${new Date().toISOString()}`,
                        msg: `${msg}`
                    }),
                    partition: 0
                  },
                ],
            })
            .then(console.log)
            .catch(e => console.error(`[example/producer] ${e.message}`, e))
    }

    const run = async () => {
        await producer.connect()
        // setInterval(sendMessage, 3000)
        sendMessage()
    }

    run().catch(e => console.error(`[example/producer] ${e.message}`, e))

    const errorTypes = ['unhandledRejection', 'uncaughtException']
    const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2']

    errorTypes.map(type => {
        process.on(type, async () => {
            try {
                console.log(`process.on ${type}`)
                await producer.disconnect()
                process.exit(0)
            } catch (_) {
                process.exit(1)
            }
        })
    })

    signalTraps.map(type => {
        process.once(type, async () => {
            try {
                await producer.disconnect()
            } finally {
                process.kill(process.pid, type)
            }
        })
    })


    // res.json([sendMessage]);
  }
}




module.exports = {
  before: {
    // all: [ authenticate('jwt') ],
    all: [],
    find: [],
    get: [],
    create: [createKafkaMessage()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

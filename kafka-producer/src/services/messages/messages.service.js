// Initializes the `messages` service on path `/messages`
const { Messages } = require("./messages.class");
const hooks = require("./messages.hooks");

module.exports = function (app) {
  const options = {
    paginate: app.get("paginate"),
  };

  //   // Initialize our service with any options it requires
  //   app.use('/messages', new Messages(options, app));

  const messages = new Messages(options, app);

  messages.docs = {
    description: "Service for user management of Kafka Producer",
    operations: {
      find: false,
      get: false,
      update: false,
      patch: false,
      remove: false,
    },
    securities: ["all"],
    definitions: {
      messages: {
        type: "object",
        required: ["topic", "payload"],
        properties: {
          topic: {
            type: "string",
            description: "The Apache Kafka topic",
          },
          payload: {
            type: "object",
            description: "The payload",
          },
        },
      },
      messages_list: {
        title: "Messages list",
        type: "array",
        //         items: {
        //           $ref: `#/definitions/messages`
        //         }
      },
    },
  };

  // Initialize our service with any options it requires
  app.use("/messages", messages);

  // Get our initialized service so that we can register hooks
  const service = app.service("messages");

  service.hooks(hooks);
};

// Initializes the `users` service on path `/users`
const { Users } = require("./users.class");
const createModel = require("../../models/users.model");
const hooks = require("./users.hooks");

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
  };

  //   // Initialize our service with any options it requires
  //   app.use('/users', new Users(options, app));

  const users = new Users(options, app);

  users.docs = {
    description: "Service for user management of Kafka Producer Middleware",
    operations: {
      find: false,
    },
    securities: ["all"],
    definitions: {
      users: {
        type: "object",
        required: ["email"],
        properties: {
          email: {
            type: "string",
            description: "The user email",
          },
          password: {
            type: "string",
            description: "The user password",
          },
          strategy: {
            type: "string",
            description: "The featherjs-swagger strategy",
          },
        },
      },
      users_list: {
        title: "Users list",
        type: "array",
        //         items: {
        //           $ref: `#/definitions/users`
        //         }
      },
    },
    //     operations: {
    //       create: {
    //         requestBody: {
    //           required: true,
    //           content: {
    //             "application/json": {
    //               schema: {
    //                 type: 'object',
    //                 properties: {
    //                   email: {
    //                     type: 'string',
    //                     description: 'The user email',
    //                   },
    //                   password: {
    //                     type: 'string',
    //                     description: 'The user password',
    //                   },
    //                   strategy: {
    //                     type: 'string',
    //                     description: 'The featherjs-swagger strategy',
    //                   },
    //                 },
    //               },
    //             },
    //           },
    //         },
    //       },
    //     },
  };

  // Initialize our service with any options it requires
  app.use("/users", users);

  // Get our initialized service so that we can register hooks
  const service = app.service("users");

  service.hooks(hooks);
};
